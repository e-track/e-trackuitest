package etrackweb.pages.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Arrays;

import static etrackweb.DriverFactory.getChromeDriver;
import static etrackweb.DriverFactory.getWebDriverWait;
import static org.junit.gen5.api.Assertions.assertEquals;

public class DashboardVerifyController {

    WebDriver driver = getChromeDriver();
    WebDriverWait wait = getWebDriverWait();

    public DashboardVerifyController KeysViewIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/keys/systems/1/interfaces/1";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController KeysImportIsDisplayed() {
        String expectedUrl = "http://symfony.e-trackweb.com/keys/import";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController userAddIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/users/create#step-1";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController userViewIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/users";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController userImportIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/users/import";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController userGroupAddIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/usergroups/add";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController userGroupViewIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/usergroups/view";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController TransactionReportIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/reports/transactions";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController AdminReportIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/reports/events";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController BookingsReportIsDisplayed() {
        String expectedUrl = "https://symfony.e-trackweb.com/reports/bookings";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController ManageVisitorsIsDisplayed() {
        String expectedUrl = "http://symfony.e-trackweb.com/visitors";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController ManageBookingsIsDisplayed() {
        String expectedUrl = "http://symfony.e-trackweb.com/bookings/manager-dashboard";
        assertEquals(expectedUrl, driver.getCurrentUrl());
        return this;
    }

    public DashboardVerifyController allPagesVisited(DashboardActController act) {
        assertEquals(19,act.getPagesVisited());
        return this;
    }



}