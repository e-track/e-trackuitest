package etrackweb.pages.dashboard;

import org.openqa.selenium.WebDriver;

import static etrackweb.DriverFactory.getChromeDriver;

public class DashboardPage {

    private DashboardActController act;
    private DashboardVerifyController verify;


    public DashboardActController act(){
        return act;
    }

    public DashboardVerifyController verify(){
        return verify;
    }

    private WebDriver driver = getChromeDriver();

    private DashboardPage(){
        // hide it
    }

    private DashboardPage(DashboardActController act, DashboardVerifyController verify){
        this.act = act;
        this.verify = verify;
    }
    public static DashboardPage getDashboardPage(){
        return new DashboardPage(new DashboardActController(),
                            new DashboardVerifyController());
    }
}
