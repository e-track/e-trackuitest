package etrackweb.pages.dashboard;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static etrackweb.DriverFactory.getChromeDriver;
import static etrackweb.DriverFactory.getWebDriverWait;
import static java.lang.Thread.sleep;


public class DashboardActController {

    private WebDriver driver = getChromeDriver();
    private WebDriverWait wait = getWebDriverWait();
    private int pagesVisited = 0;

    public int getPagesVisited() {
        return pagesVisited;
    }

    public void setPagesVisited(int pagesVisited) {
        this.pagesVisited = pagesVisited;
    }



    public DashboardActController selectKeysMenu() {
        driver.manage().window().setSize(new Dimension(1297, 1020));
        driver.findElement(By.cssSelector(".nav-item:nth-child(4) > .collapsed > span")).click();
        driver.findElement(By.linkText("View Keys")).click();
        return this;
    }

    public DashboardActController selectImportKeys() {
        driver.findElement(By.linkText("Keys")).click();
        driver.findElement(By.linkText("Import Keys")).click();
        return this;
    }

    public DashboardActController selectUsersAddMenu() {
        driver.findElement(By.linkText("Users")).click();
        driver.findElement(By.linkText("Add User")).click();
        return this;
    }

    public DashboardActController selectUsersViewMenu() throws InterruptedException {
        driver.findElement(By.linkText("View Users")).click();
        driver.findElement(By.cssSelector(".nav-item:nth-child(6) span")).click();
        return this;
    }

    public DashboardActController selectUsersImportMenu() {
        driver.findElement(By.cssSelector(".nav-item:nth-child(6) span")).click();
        driver.findElement(By.linkText("Import Users")).click();
        return this;
    }

    public DashboardActController selectUserGroupsAddMenu() {
        driver.findElement(By.cssSelector(".nav-item:nth-child(6) span")).click();
        driver.findElement(By.linkText("Add User Groups")).click();
        return this;
    }

    public DashboardActController selectUserGroupsViewMenu() {
        driver.findElement(By.cssSelector(".nav-item:nth-child(6) span")).click();
        driver.findElement(By.linkText("View User Groups")).click();
        return this;
    }

    public DashboardActController selectTransactionReport() {
        driver.findElement(By.cssSelector(".nav-item:nth-child(8) span")).click();
        driver.findElement(By.linkText("Transaction Report")).click();
        return this;
    }

    public DashboardActController selectAdminReport() {
        driver.findElement(By.cssSelector(".nav-item:nth-child(8) span")).click();
        driver.findElement(By.linkText("Admin Report")).click();
        return this;
    }

    public DashboardActController selectBookingsReport() {
        driver.findElement(By.cssSelector(".nav-item:nth-child(8) span")).click();
        driver.findElement(By.linkText("Bookings Report")).click();
        return this;
    }

    public int navigationLinks() {
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(4) > .collapsed > span")).click();
        driver.findElement(By.linkText("View Keys")).click();
        pagesVisited++;
        driver.findElement(By.linkText("Keys")).click();
        driver.findElement(By.linkText("Import Keys")).click();
        pagesVisited++;
        driver.findElement(By.linkText("Users")).click();
        driver.findElement(By.linkText("Add User")).click();
        pagesVisited++;
        driver.findElement(By.linkText("Users")).click();
        driver.findElement(By.linkText("View Users")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(6) span")).click();
        driver.findElement(By.linkText("Import Users")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(6) span")).click();
        driver.findElement(By.linkText("Add User Groups")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(6) span")).click();
        driver.findElement(By.linkText("View User Groups")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(8) span")).click();
        driver.findElement(By.linkText("Transaction Report")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(8) span")).click();
        driver.findElement(By.linkText("Admin Report")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(8) span")).click();
        driver.findElement(By.linkText("Bookings Report")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(10) span")).click();
        driver.findElement(By.linkText("Manage Visitors")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(10) span")).click();
        driver.findElement(By.linkText("Manage Bookings")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(10) span")).click();
        driver.findElement(By.linkText("Create Booking(s)")).click();
        pagesVisited++;
        driver.findElement(By.linkText("Settings")).click();
        driver.findElement(By.linkText("General Settings")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(12) span")).click();
        driver.findElement(By.linkText("Email Settings")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(12) span")).click();
        driver.findElement(By.linkText("Profile Settings")).click();
        pagesVisited++;
        driver.findElement(By.cssSelector(".nav-item:nth-child(12) span")).click();
        driver.findElement(By.linkText("Booking Settings")).click();
        pagesVisited++;
        driver.findElement(By.linkText("Settings")).click();
        driver.findElement(By.linkText("Engineer Settings")).click();
        return pagesVisited++;
    }



   }

