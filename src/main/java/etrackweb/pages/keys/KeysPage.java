package etrackweb.pages.keys;

import etrackweb.pages.dashboard.DashboardActController;
import etrackweb.pages.dashboard.DashboardPage;
import etrackweb.pages.dashboard.DashboardVerifyController;
import org.openqa.selenium.WebDriver;

import static etrackweb.DriverFactory.getChromeDriver;

public class KeysPage {
    private KeysActController act;
    private KeysVerifyController verify;

    public KeysActController act(){
        return act;
    }

    public KeysVerifyController verify(){
        return verify;
    }

    private WebDriver driver = getChromeDriver();

    private KeysPage(){
        // hide it
    }

    private KeysPage(KeysActController act, KeysVerifyController verify){
        this.act = act;
        this.verify = verify;
    }
    public static KeysPage getKeysPage(){
        return new KeysPage(new KeysActController(),
                new KeysVerifyController());
    }
}
