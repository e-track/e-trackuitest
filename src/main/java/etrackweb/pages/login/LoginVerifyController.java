package etrackweb.pages.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


import static etrackweb.DriverFactory.getChromeDriver;
import static etrackweb.DriverFactory.getWebDriverWait;
import static org.junit.gen5.api.Assertions.assertEquals;

public class LoginVerifyController {

    WebDriver driver = getChromeDriver();
    WebDriverWait wait = getWebDriverWait();

    public LoginVerifyController userCanLogin() {
        String expectedUrl = "https://apidemo.keytracker.com/dashboard";
        String actualUrl = driver.getCurrentUrl();
        assertEquals(actualUrl,expectedUrl);
        return this;
    }


}
