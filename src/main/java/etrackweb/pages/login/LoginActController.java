package etrackweb.pages.login;

import etrackweb.pages.dashboard.DashboardPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static etrackweb.DriverFactory.getChromeDriver;
import static etrackweb.DriverFactory.getWebDriverWait;

public class LoginActController {

    WebDriver driver = getChromeDriver();
    WebDriverWait wait = getWebDriverWait();


    public LoginActController enterUserDetails(String user,String pass) {
        WebElement username = driver.findElement(By.id("username"));
        WebElement password = driver.findElement(By.id("password"));
        username.sendKeys(user);
        password.sendKeys(pass);
        return this;
    }

    public DashboardPage loginUser() {
        WebElement login = driver.findElement(By.id("submit"));
        login.click();
        return DashboardPage.getDashboardPage();
    }
}
