package etrackweb.pages.login;

import org.openqa.selenium.WebDriver;

import static etrackweb.DriverFactory.getChromeDriver;

public class LoginPage {
    private LoginActController act;
    private LoginVerifyController verify;

    public LoginActController act(){
        return act;
    }

    public LoginVerifyController verify(){
        return verify;
    }

    private WebDriver driver = getChromeDriver();

    private LoginPage(LoginActController act, LoginVerifyController verify){
        this.act = act;
        this.verify = verify;
    }
    public static LoginPage getLoginPage(){
        return new LoginPage(new LoginActController(),
                new LoginVerifyController());
    }
}
