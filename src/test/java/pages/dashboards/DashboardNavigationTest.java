package pages.dashboards;

import etrackweb.pages.dashboard.DashboardPage;
import etrackweb.pages.login.LoginPage;
import org.junit.jupiter.api.Test;
import pages.BaseTestClass;

public class DashboardNavigationTest  extends BaseTestClass {

    DashboardPage dashboardPage = DashboardPage.getDashboardPage();
    LoginPage loginPage = LoginPage.getLoginPage();

    @Test
    public void allviewsAccessibleTest() throws InterruptedException {

        loginPage.act()
                .enterUserDetails("eTrackEngineer","eTrack3010")
                .loginUser();

       dashboardPage.act()
                .navigationLinks();

       dashboardPage.verify()
               .allPagesVisited(dashboardPage.act());
    }
}
