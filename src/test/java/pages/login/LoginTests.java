package pages.login;

import etrackweb.pages.login.LoginPage;
import org.junit.jupiter.api.Test;
import pages.BaseTestClass;

public class LoginTests extends BaseTestClass {

    LoginPage loginPage = LoginPage.getLoginPage();

    @Test
    public void LoginSuccessfully() {

        loginPage.act()
                .enterUserDetails("eTrackEngineer","eTrack3010")
                .loginUser();

        loginPage.verify()
                .userCanLogin();
    }

}
