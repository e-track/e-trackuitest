package pages;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.concurrent.TimeUnit;

import static etrackweb.DriverFactory.getChromeDriver;
import static etrackweb.DriverFactory.getWebDriverWait;

public class BaseTestClass {

    static WebDriver driver;
    static WebDriverWait wait;

    @BeforeAll
    public static void startUpBrowser() {

        driver = getChromeDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().fullscreen();
        wait = getWebDriverWait();
    }

    @BeforeEach
    public void goToHome() {
        driver.get("https://symfony.e-trackweb.com/login");
    }

    @AfterAll
    public static void closeBrowser() {
        driver.close();
    }

}
